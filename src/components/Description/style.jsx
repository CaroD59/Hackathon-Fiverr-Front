import styled from 'styled-components';

const StyledDesription = styled.div`
  display: flex;
  flex-direction: row-reverse;
  color: #f5f7f7;

  .Description {
    display: flex;
    color: #000000;
  }
  .textDesciption {
    width: 50vw;
    text-align: left;
  }
  .imageDescription {
    width: 44vw;
    margin-right: 5vw;
    margin-left: 10vw;
  }
`;
export default StyledDesription;
