import Comments from '../Comments/Comments';
import InputComments from '../Comments/InPutComments';

export default function CardsDescription() {
  return (
    <div className="CommentsPart">
      <Comments />
      <InputComments />
    </div>
  );
}
